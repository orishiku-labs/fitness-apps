""" Views module
"""
from dacommon.views import BaseView


class HomeView(BaseView):
    """Home view"""

    template_name = "home.html"

    def dispatch(self, request, *args, **kwargs):
        self.add_data("site_title", "Curious data")
        return BaseView.dispatch(self, request, *args, **kwargs)
