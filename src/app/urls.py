""" Base urls module
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from django.conf.urls.static import static
from django.conf import settings
from app.views import HomeView
from calories.views import CaloriesView

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("", include("dafitness.urls")),
    path("calories", CaloriesView.as_view(), name="calories"),
    path("admin/", admin.site.urls),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
