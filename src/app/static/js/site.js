 // Define the function 
// to screenshot the div
function takeshot() {
    let div = document.getElementsByClassName('ticket').item(0);

    // Use the html2canvas
    // function to take a screenshot
    // and append it
    // to the output div
    html2canvas(div).then(
        function (canvas) {
            download(canvas)
        })
}

const download = function (canvas) {
    const link = document.createElement('a');
    link.download = 'calories-ticket.png';
    link.href = canvas.toDataURL()
    link.click();
}