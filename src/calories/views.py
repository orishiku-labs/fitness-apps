""" Views module
"""
from dacommon.integrations.bases import BaseAppView
from dacommon.utils.datetime import PeriodName, get_period
from dafitness.integrations.strava import StravaIntegration, Scopes
from dafitness.integrations.utils import CaloriesConverter
from calories.utils import get_random_data


class CaloriesView(BaseAppView):
    """Home view"""

    template_name = "calories.html"
    integration_class = StravaIntegration
    integration_scopes = [
        Scopes.ACTIVITY_READ,
    ]

    def response_data(self, request):
        from_date, to_date = get_period(period_unit=PeriodName.WEEK)
        user = self.integration.athlete
        activity_list = self.integration.activities(
            100, 1, from_date=from_date, to_date=to_date
        )
        data = {"calories": 0, "items": []}
        for item in activity_list:
            activity = self.integration.activity(item["id"])
            data["calories"] += activity["calories"]

        calories = CaloriesConverter(data["calories"], CaloriesConverter.Unit.CAL)
        data["calories"] = int(calories.get_calories())
        data["items"] = get_random_data(data["calories"])
        data["user"] = (user["username"],)
        self.add_data("data", data)

    def dispatch(self, request, *args, **kwargs):
        self.add_data("site_title", "Curious data")
        self.add_data(
            "external_scripts",
            [
                "https://html2canvas.hertzen.com/dist/html2canvas.min.js",
            ],
        )
        return BaseAppView.dispatch(self, request, *args, **kwargs)
