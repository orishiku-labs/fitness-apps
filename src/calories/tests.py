""" Tests module
"""
import random
from django.test import TestCase
from calories.utils import random_segments


class UtilsTestCase(TestCase):
    """Utils test case"""

    def test_random_segments(self):
        """Tests random segments method"""
        for _ in range(100):
            max_size = random.randint(2000, 100000)
            max_segments_count = random.randint(1, 10)
            segments = random_segments(max_size, max_segments_count)
            segments_total = 0
            for item in segments:
                segments_total += item

            self.assertEqual(segments_total, max_size)
            self.assertEqual(len(segments), max_segments_count)
