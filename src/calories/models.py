""" Models module
"""
from django.db import models


class RandomData(models.Model):
    """Random data comparisons model"""

    description = models.CharField(max_length=100)
    calories = models.FloatField()

    class Meta:
        verbose_name = "Random Data"
        verbose_name_plural = "Random Data"

    def __str__(self):
        return self.description
