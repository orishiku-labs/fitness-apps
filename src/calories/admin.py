""" Admin module
"""
from django.contrib import admin
from calories.models import RandomData


@admin.register(RandomData)
class RandomDataAdmin(admin.ModelAdmin):
    """Random data admin class"""

    list_display = ["description", "calories"]
