""" Apps module
"""
from django.apps import AppConfig


class CaloriesConfig(AppConfig):
    """calories configuration class"""

    default_auto_field = "django.db.models.BigAutoField"
    name = "calories"
