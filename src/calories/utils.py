""" Calories app utils module
"""
import random
from decimal import Decimal, getcontext, ROUND_UP
from calories.models import RandomData


def get_random_data(total_calories):
    """Returns random data list to cover total calories value"""
    segments = random_segments(total_calories, 6)
    items = list(RandomData.objects.all().values_list("pk", flat=True))
    items = random.sample(items, len(segments))
    data = []

    for i, index in enumerate(items):
        item = RandomData.objects.get(pk=index)
        item_count = segments[i] / Decimal(item.calories)

        zero_count = count_decimal_zeros(item_count)

        getcontext().prec = 2 + zero_count
        getcontext().rounding = ROUND_UP

        item_count += Decimal(0)

        item_data = (
            item_count if item_count > 0 else "#?",
            item.description,
            int(segments[i]),
        )
        data.append(item_data)

    return data


def count_decimal_zeros(decimal: Decimal):
    """Returns zeros count after decimal point"""
    decimal = decimal.as_tuple()
    zero_count = 0
    for digit in decimal.digits[decimal.exponent :]:
        if digit == 0:
            zero_count += 1
        else:
            break

    return zero_count


def random_segments(max_size, max_segments) -> list:
    """returns a list of segments that covers max value"""
    segments = []
    remainder_percent = 100
    for _ in range(100):
        segment_size = random.randint(10, 40)
        remainder_percent -= segment_size

        if remainder_percent < 0:
            segment_size += remainder_percent
        segments.append(Decimal(segment_size) / Decimal(100))

        if remainder_percent < 0:
            break

    segments = [Decimal(max_size) * segment for segment in segments]

    if len(segments) > max_segments:
        temp_seg = segments.copy()
        temp_seg.sort(reverse=True)
        temp_seg = temp_seg[max_segments:]
        for item in temp_seg:
            segments.pop(item)

        for i, value in enumerate(temp_seg):
            segments[i] += value

    return segments
