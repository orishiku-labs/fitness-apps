FROM python:3.10.11-slim

ARG REGISTRY_PYPI
ENV PYTHONFAULTHANDLER=1 \
  	PYTHONUNBUFFERED=1 \
  	PYTHONHASHSEED=random \
  	PYTHONDONTWRITEBYTECODE=1 \
  	# pip:
  	PIP_NO_CACHE_DIR=1 \
  	PIP_DISABLE_PIP_VERSION_CHECK=1 \
  	PIP_DEFAULT_TIMEOUT=100 \
  	PIP_ROOT_USER_ACTION=ignore \
  	# poetry:
  	POETRY_VERSION=1.4.2 \
	PATH=/usr/lib/postgresql/X.Y/bin/:/.venv/bin:$PATH \
  	# app:
  	APP_PATH='/app'

ADD src $APP_PATH
ADD pyproject.toml /

RUN pip install poetry==${POETRY_VERSION}
RUN poetry --version
RUN apt-get update
RUN apt-get install -y build-essential \
		zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev \
		libssl-dev libreadline-dev libffi-dev curl libsqlite3-dev \
		python3-dev libpq-dev
# Cleaning cache:
RUN apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false
RUN apt-get clean -y

# Project initialization:
RUN poetry config http-basic.softapricot registry_access ${REGISTRY_PYPI}

RUN poetry config virtualenvs.create true \
 	&& poetry config virtualenvs.in-project true \
 	&& poetry install --without dev --with dist --no-interaction --no-ansi \
  	&& mkdir -p $APP_PATH/static

WORKDIR $APP_PATH

EXPOSE 8000

ENTRYPOINT ["sh", "start.sh"]