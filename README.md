# Fitness Apps : v2023.0.8

Web apps that displays fun facts based on fitness stats

## Development

To prepare develoment environment use poetry

```console
$ pip install poetry==1.4.2
$ poetry install
$ poetry self add poetry-bumpversion
```

To update version across all references use the following command:

```console
$ poetry version <bump rule>
```

### Available bump rules:

* patch
* minor
* major
* prepatch
* preminor
* premajor
* prerelease

## Running Tests

```console
$ poetry run tox -e <python version>-<testenv name>
```

### Available python versions:

* py39
* py310
* py311

### Available testenvs:

* pylint
* flake8
* build
* tests
