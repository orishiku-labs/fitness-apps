# Change Log

## [2023.0.8] - 2023-07-01

### Added
- umami analytics script

## [2023.0.6] - 2023-06-31

### Changed
- set ticket items limit to 6
- style fixes for better sharing

## [2023.0.5] - 2023-06-30

### Changed
- ticket style
- removed `only_full_units` field

## [2023.0.4] - 2023-06-30

### Added
- button that download ticket in png

## [2023.0.3] - 2023-06-30

### Fixed
- fixed `Calories` app's calories calculations

## [2023.0.1] - 2023-06-28

### Changed
- `unit` field removed from `RandomDataModel`
- `only_full_units` field added to `RandomDataModel`
- better style template on calories app

## [2023.0.0] - 2023-06-28

### Added
- Base project structure
- Calories app
