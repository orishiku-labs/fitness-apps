image: python:3.10-slim

stages:
  - analysis
  - tests
  - deploy

.before_script: &python_environment
  before_script:
    - python -V
    - pip install poetry==1.4.2 tox
    - poetry version
    - poetry config virtualenvs.in-project true
    - poetry config http-basic.softapricot registry_access ${REGISTRY_PYPI}
    - poetry install --without dist
  cache:
    paths:
      - .venv

flake8:
  stage: analysis
  <<: *python_environment
  script:
    - poetry run tox -e flake8
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      changes:
        paths:
          - src/app/**/*

pylint:
  stage: analysis
  needs: [flake8,]
  <<: *python_environment
  script:
    - poetry run tox -e pylint
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      changes:
        paths:
          - src/app/**/*

coverage:
  stage: analysis
  <<: *python_environment
  script:
    - poetry run tox -e coverage
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      needs: [pylint,]
      changes:
        paths:
          - src/app/**/*
      when: on_success

prepare_release:
  stage: deploy
  <<: *python_environment
  script:
    - echo "APP_VERSION=$(poetry version -s)" >> build.env
  artifacts:
    reports:
      dotenv: build.env
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - src/app/__init__.py

build_release:
  image: docker:19.03.1
  stage: deploy
  needs: [prepare_release,]
  services:
    - docker:19.03.1-dind
  before_script:
    - export DOCKER_REGISTRY_USER=$CI_REGISTRY_USER # built-in GitLab Registry User
    - export DOCKER_REGISTRY_PASSWORD=$CI_REGISTRY_PASSWORD # built-in GitLab Registry Password
    - export DOCKER_REGISTRY_URL=$CI_REGISTRY # built-in GitLab Registry URL
    - export COMMIT_HASH=$CI_COMMIT_SHA # Your current commit sha
    - export IMAGE_NAME_WITH_REGISTRY_PREFIX=$CI_REGISTRY_IMAGE # Your repository prefixed with GitLab Registry URL
    - docker login -u "$DOCKER_REGISTRY_USER" -p "$DOCKER_REGISTRY_PASSWORD" $DOCKER_REGISTRY_URL # Instructs GitLab to login to its registry

  script:
    - echo "running build job for $APP_VERSION"
    - export CONTAINER_FULL_IMAGE_NAME_WITH_TAG=$IMAGE_NAME_WITH_REGISTRY_PREFIX/img-captain-fitness:$COMMIT_HASH
    - docker build -f ./Dockerfile --pull -t img-captain-fitness . --build-arg REGISTRY_PYPI=$REGISTRY_PYPI
    - docker tag img-captain-fitness "$CONTAINER_FULL_IMAGE_NAME_WITH_TAG"
    - docker push "$CONTAINER_FULL_IMAGE_NAME_WITH_TAG"
    - echo $CONTAINER_FULL_IMAGE_NAME_WITH_TAG
    - echo "Deploying on CapRover..."
    - docker run caprover/cli-caprover:v2.1.1 caprover deploy --caproverUrl $CAPROVER_URL --caproverPassword $CAPROVER_PASSWORD --caproverApp $CAPROVER_APP --imageName $CONTAINER_FULL_IMAGE_NAME_WITH_TAG
    - echo "APP_VERSION=$APP_VERSION" >> build.env
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - src/app/__init__.py
      when: on_success

  artifacts:
    reports:
      dotenv: build.env

release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs: [build_release,]
  script:
    - echo "running release job for $APP_VERSION"
  release:
    tag_name: 'v$APP_VERSION'
    description: 'v$APP_VERSION'
    ref: '$CI_COMMIT_SHA'
  environment:
    name: production
    url: https://orishiku.com
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - src/app/__init__.py
      when: on_success
